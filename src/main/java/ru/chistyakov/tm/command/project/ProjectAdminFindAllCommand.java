package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public class ProjectAdminFindAllCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pafac";
    }

    @Override
    public @NotNull String getDescription() {
        return "Выводит все проекты всех пользователей";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException, JAXBException, ClassNotFoundException {
        for (final Project project : serviceLocator.getProjectService().findAll()) System.out.println(project);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
