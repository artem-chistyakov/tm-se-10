package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public class ProjectFindAllOrderDateBeginCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pfadbc";
    }

    @Override
    public @NotNull String getDescription() {
        return "Выводит проекты в порядке даты начала";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        Collection<Project> projectCollection = serviceLocator.getProjectService().findAllInOrderDateBegin(serviceLocator.getCurrentUser().getId());
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (Project project : projectCollection) System.out.println(project);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR, RoleType.USUAL_USER};
    }
}
