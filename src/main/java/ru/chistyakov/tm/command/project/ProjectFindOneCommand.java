package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;


public class ProjectFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pfoc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск одного проекта по id у авторизованного пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        Project project = serviceLocator.getProjectService().findOne(serviceLocator.getCurrentUser().getId(), projectId);
        if (project == null) throw new IllegalArgumentException("Проект не найден");
        else
            System.out.println(serviceLocator.getProjectService().findOne(serviceLocator.getCurrentUser().getId(), projectId));
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
