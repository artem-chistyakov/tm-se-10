package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public class ProjectFindAllOrderDateEndCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "pfadec";
    }

    @Override
    public @NotNull String getDescription() {
        return "выводит все проекты в порядке дня окончания";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        Collection<Project> projectCollection = serviceLocator.getProjectService().findAllInOrderDateEnd(serviceLocator.getCurrentUser().getId());
        if (projectCollection.isEmpty()) throw new IllegalArgumentException("Проекты не найдены");
        for (Project project : projectCollection) System.out.println(project);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
