package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public class UserFindAll extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "ufa";
    }

    @Override
    public @NotNull String getDescription() {
        return "Выводит все профили пользователей";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException, JAXBException, ClassNotFoundException {
        for (final User user : serviceLocator.getUserService().findAll()) System.out.println(user);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
