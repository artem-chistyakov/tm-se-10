package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class UserSerializateDomen extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "usd";
    }

    @Override
    public @NotNull String getDescription() {
        return "Сериализация доменной области";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException {
        try (final FileOutputStream projectFileOutputStream = new FileOutputStream("src/main/file/fileForProjects");
             final FileOutputStream taskFileOutputStream = new FileOutputStream("src/main/file/fileForTasks");
             final FileOutputStream userFileOutputStream = new FileOutputStream("src/main/file/fileForUsers")) {
            final ObjectOutputStream projectObjectOutputStream = new ObjectOutputStream(projectFileOutputStream);
            projectObjectOutputStream.writeObject(serviceLocator.getProjectService().findAll());
            final ObjectOutputStream taskObjectOutputStream = new ObjectOutputStream(taskFileOutputStream);
            taskObjectOutputStream.writeObject(serviceLocator.getTaskService().findAll());
            final ObjectOutputStream userObjectOutputStream = new ObjectOutputStream(userFileOutputStream);
            userObjectOutputStream.writeObject(serviceLocator.getUserService().findAll());
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
