package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.wrapper.Projects;
import ru.chistyakov.tm.wrapper.Tasks;
import ru.chistyakov.tm.wrapper.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.List;

public class UserSaveDomenXml extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "usdx";
    }

    @Override
    public @NotNull String getDescription() {
        return "Сохранение доменной области в формате xml";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, JAXBException {
        final JAXBContext projectJaxbContext = JAXBContext.newInstance(Projects.class);
        final Marshaller projectMarshaller = projectJaxbContext.createMarshaller();
        final JAXBContext taskJaxbContext = JAXBContext.newInstance(Tasks.class);
        final Marshaller taskMarshaller = taskJaxbContext.createMarshaller();
        final JAXBContext userJaxbContext = JAXBContext.newInstance(Users.class);
        final Marshaller userMarshaller = userJaxbContext.createMarshaller();
        final Projects projects = new Projects();
        final Tasks tasks = new Tasks();
        final Users users = new Users();
        projects.setProjects((List<Project>) serviceLocator.getProjectService().findAll());
        tasks.setTasks((List<Task>) serviceLocator.getTaskService().findAll());
        users.setUsers((List<User>) serviceLocator.getUserService().findAll());
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        projectMarshaller.marshal(projects, new File("src/main/file/projects.xml"));
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        taskMarshaller.marshal(tasks, new File("src/main/file/tasks.xml"));
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        userMarshaller.marshal(users, new File("src/main/file/users.xml"));
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
