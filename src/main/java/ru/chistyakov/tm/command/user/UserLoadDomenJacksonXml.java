package ru.chistyakov.tm.command.user;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public class UserLoadDomenJacksonXml extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "uldjx";
    }

    @Override
    public @NotNull String getDescription() {
        return "загрузка предметной области с использованием jackson в формате xml";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException {
        final JacksonXmlModule jacksonXmlModule = new JacksonXmlModule();
        jacksonXmlModule.setDefaultUseWrapper(false);
        final XmlMapper xmlMapper = new XmlMapper(jacksonXmlModule);
        final Iterator<Project> projectIterator = xmlMapper.readerFor(Project.class).readValues(new File("src/main/file/projectsF.xml"));
        final Iterator<Task> taskIterator = xmlMapper.readerFor(Task.class).readValues(new File("src/main/file/tasksF.xml"));
        final Iterator<User> userIterator = xmlMapper.readerFor(User.class).readValues(new File("src/main/file/usersF.xml"));
        while (userIterator.hasNext()) {
            serviceLocator.getUserService().persist(userIterator.next());
        }
        while (projectIterator.hasNext()) {
            serviceLocator.getProjectService().persist(projectIterator.next());
        }
        while (taskIterator.hasNext()) {
            serviceLocator.getTaskService().persist(taskIterator.next());
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
