package ru.chistyakov.tm.command.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

public class UserLoadDomenJacksonJson extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "uldjj";
    }

    @Override
    public @NotNull String getDescription() {
        return "загрузка предметной области с использование jackson в формате json";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException {
        final ObjectMapper objectMapper = new ObjectMapper();
        final Iterator<Project> projectIterator = objectMapper.readerFor(Project.class).readValues(new File("src/main/file/projectsF.json"));
        final Iterator<Task> taskIterator = objectMapper.readerFor(Task.class).readValues(new File("src/main/file/tasksF.json"));
        final Iterator<User> userIterator = objectMapper.readerFor(User.class).readValues(new File("src/main/file/usersF.json"));
        while (userIterator.hasNext()) {
            serviceLocator.getUserService().persist(userIterator.next());
        }
        while (projectIterator.hasNext()) {
            serviceLocator.getProjectService().persist(projectIterator.next());
        }
        while (taskIterator.hasNext()) {
            serviceLocator.getTaskService().persist(taskIterator.next());
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
