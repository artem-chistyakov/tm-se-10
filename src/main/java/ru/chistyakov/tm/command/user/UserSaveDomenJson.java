package ru.chistyakov.tm.command.user;

import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.wrapper.Projects;
import ru.chistyakov.tm.wrapper.Tasks;
import ru.chistyakov.tm.wrapper.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.util.List;

public class UserSaveDomenJson extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "usdj";
    }

    @Override
    public @NotNull String getDescription() {
        return "Экстернализация предметной области в формате json";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, JAXBException {
        final JAXBContext projectJaxbContext = JAXBContext.newInstance(Projects.class);
        final Marshaller projectMarshaller = projectJaxbContext.createMarshaller();
        final JAXBContext taskJaxbContext = JAXBContext.newInstance(Tasks.class);
        final Marshaller taskMarshaller = taskJaxbContext.createMarshaller();
        final JAXBContext userJaxbContext = JAXBContext.newInstance(Users.class);
        final Marshaller userMarshaller = userJaxbContext.createMarshaller();
        projectMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        projectMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        taskMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        taskMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        userMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, "application/json");
        userMarshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, true);
        final Projects projects = new Projects();
        final Tasks tasks = new Tasks();
        final Users users = new Users();
        projects.setProjects((List<Project>) serviceLocator.getProjectService().findAll());
        tasks.setTasks((List<Task>) serviceLocator.getTaskService().findAll());
        users.setUsers((List<User>) serviceLocator.getUserService().findAll());
        projectMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        projectMarshaller.marshal(projects, new File("src/main/file/projects.json"));
        taskMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        taskMarshaller.marshal(tasks, new File("src/main/file/tasks.json"));
        userMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        userMarshaller.marshal(users, new File("src/main/file/users.json"));
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
