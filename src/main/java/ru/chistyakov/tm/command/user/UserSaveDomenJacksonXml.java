package ru.chistyakov.tm.command.user;

import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

import java.io.FileOutputStream;
import java.io.IOException;

public class UserSaveDomenJacksonXml extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "usdjx";
    }

    @Override
    public @NotNull String getDescription() {
        return "Сохранение предметной области в формате xml с помощью fasterxml";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException {
        try (FileOutputStream fOSProject = new FileOutputStream("src/main/file/projectsF.xml");
             FileOutputStream fOSTask = new FileOutputStream("src/main/file/tasksF.xml");
             FileOutputStream fOSUser = new FileOutputStream("src/main/file/usersF.xml")) {
            final JacksonXmlModule module = new JacksonXmlModule();
            module.setDefaultUseWrapper(false);
            final XmlMapper xmlMapper = new XmlMapper(module);
            final String projectJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getProjectService().findAll());
            final String taskJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getTaskService().findAll());
            final String userJsonArray = xmlMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getUserService().findAll());
            fOSProject.write(projectJsonArray.getBytes());
            fOSProject.flush();
            fOSTask.write(taskJsonArray.getBytes());
            fOSTask.flush();
            fOSUser.write(userJsonArray.getBytes());
            fOSUser.flush();
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
