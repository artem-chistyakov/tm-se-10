package ru.chistyakov.tm.command.user;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

import java.io.FileOutputStream;
import java.io.IOException;

public class UserSaveDomenJacksonJson extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "usdjj";
    }

    @Override
    public @NotNull String getDescription() {
        return "Сохранение предметной области в формате json с помощью fasterxml";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException {
        try (FileOutputStream fOSProject = new FileOutputStream("src/main/file/projectsF.json");
             FileOutputStream fOSTask = new FileOutputStream("src/main/file/tasksF.json");
             FileOutputStream fOSUser = new FileOutputStream("src/main/file/usersF.json")) {
            final ObjectMapper objectMapper = new ObjectMapper();
            final String projectJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getProjectService().findAll());
            final String taskJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getTaskService().findAll());
            final String userJsonArray = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(serviceLocator.getUserService().findAll());
            fOSProject.write(projectJsonArray.getBytes());
            fOSTask.write(taskJsonArray.getBytes());
            fOSUser.write(userJsonArray.getBytes());
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
