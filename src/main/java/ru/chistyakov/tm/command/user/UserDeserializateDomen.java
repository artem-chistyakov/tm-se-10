package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.Collection;

public class UserDeserializateDomen extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "udd";
    }

    @Override
    public @NotNull String getDescription() {
        return "Загрузка предметной области с использованием десериализации";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException, ClassNotFoundException {
        try (final FileInputStream userFileInputStream = new FileInputStream("src/main/file/fileForUsers");
             final FileInputStream projectFileInputStream = new FileInputStream("src/main/file/fileForProjects");
             final FileInputStream taskFileInputStream = new FileInputStream("src/main/file/fileForTasks")) {
            final ObjectInputStream userObjectInputStream = new ObjectInputStream(userFileInputStream);
            final ObjectInputStream projectObjectInputStream = new ObjectInputStream(projectFileInputStream);
            final ObjectInputStream taskObjectInputStream = new ObjectInputStream(taskFileInputStream);
            final Collection<User> userCollection = (Collection<User>) userObjectInputStream.readObject();
            for (final User user : userCollection) {
                serviceLocator.getUserService().persist(user);
            }
            final Collection<Project> projectCollection = (Collection<Project>) projectObjectInputStream.readObject();
            for (final Project project : projectCollection) {
                serviceLocator.getProjectService().persist(project);
            }
            final Collection<Task> taskCollection = (Collection<Task>) taskObjectInputStream.readObject();
            for (Task task : taskCollection) {
                serviceLocator.getTaskService().persist(task);
            }
        }
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
