package ru.chistyakov.tm.command.user;

import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;
import ru.chistyakov.tm.wrapper.Projects;
import ru.chistyakov.tm.wrapper.Tasks;
import ru.chistyakov.tm.wrapper.Users;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class UserLoadDomenJson extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "uldj";
    }

    @Override
    public @NotNull String getDescription() {
        return "загрузка предметной области с использованием Jax-b в формате json";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, JAXBException {
        final JAXBContext projectJaxbContext = JAXBContext.newInstance(Projects.class);
        final JAXBContext tasksJaxbContext = JAXBContext.newInstance(Tasks.class);
        final JAXBContext usersJaxbContext = JAXBContext.newInstance(Users.class);
        Unmarshaller projectUnmarshaller = projectJaxbContext.createUnmarshaller();
        projectUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        projectUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        Unmarshaller taskUnmarshaller = tasksJaxbContext.createUnmarshaller();
        taskUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        taskUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        Unmarshaller userUnmarshaller = usersJaxbContext.createUnmarshaller();
        userUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, "application/json");
        userUnmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, true);
        final Projects projects = (Projects) projectUnmarshaller.unmarshal(new File("src/main/file/projects.json"));
        final Tasks tasks = (Tasks) taskUnmarshaller.unmarshal(new File("src/main/file/tasks.json"));
        final Users users = (Users) userUnmarshaller.unmarshal(new File("src/main/file/users.json"));
        for (User user : users.getUsers()) serviceLocator.getUserService().persist(user);
        for (Project project : projects.getProjects()) serviceLocator.getProjectService().persist(project);
        for (Task task : tasks.getTasks()) serviceLocator.getTaskService().persist(task);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
