package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public class TaskAdminFindAllCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "tafac";
    }

    @Override
    public @NotNull String getDescription() {
        return "Выводит все задачи всех пользователей";
    }

    @Override
    public void execute() throws IllegalArgumentException, NullPointerException, IOException, JAXBException, ClassNotFoundException {
        for (final Task task : serviceLocator.getTaskService().findAll()) System.out.println(task);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR};
    }
}
