package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public class TaskFindAllOrderDateEndCommand extends AbstractCommand {
    @Override
    public @NotNull String getName() {
        return "tfadec";
    }

    @Override
    public @NotNull String getDescription() {
        return "выводит все задачи в порядке даты окончания";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        Collection<Task> collection = serviceLocator.getTaskService().findAllInOrderDateEnd(serviceLocator.getCurrentUser().getId());
        if (collection.isEmpty()) throw new IllegalArgumentException("Задачи не найдены");
        for (Task task : collection) System.out.println(task);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ADMINISTRATOR, RoleType.USUAL_USER};
    }
}
