package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;


public class TaskFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tfoc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск одной команды авторизованного пользователя";
    }

    @Override
    public void execute() {
        if (serviceLocator.getCurrentUser() == null)
            throw new NullPointerException("Текущий пользователь не определен");
        System.out.println("Введите id задачи");
        final Task task = serviceLocator.getTaskService().findOne(serviceLocator.getCurrentUser().getId(), serviceLocator.getScanner().nextLine());
        if (task == null) throw new IllegalArgumentException("Ошибка поиска задачи");
        System.out.println(task);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
