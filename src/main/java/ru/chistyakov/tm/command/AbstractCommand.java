package ru.chistyakov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.ServiceLocator;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.xml.bind.JAXBException;
import java.io.IOException;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute() throws IllegalArgumentException, NullPointerException, IOException, JAXBException, ClassNotFoundException;

    @NotNull
    public abstract RoleType[] getSupportedRoles();
}
