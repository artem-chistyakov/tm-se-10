package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.IRepository;
import ru.chistyakov.tm.entity.AbstractEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;


public abstract class AbstractRepository<T extends AbstractEntity> implements IRepository<T> {

    final Map<String, T> entities = new LinkedHashMap<>();

    public Map<String, T> getEntities() {
        return entities;
    }

    public abstract boolean persist(T t);

    public abstract boolean merge(T t);

    @NotNull
    public Collection<T> findAll() {
        return new ArrayList<>(entities.values());
    }

}
