package ru.chistyakov.tm.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserRepository;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean insert(@NotNull final String login, @NotNull final String password) {
        if (findByLogin(login) != null) return false;
        final User user = new User();
        if (entities.containsKey(user.getId())) return false;
        user.setRoleType(RoleType.USUAL_USER);
        user.setLogin(login);
        user.setPassword(password);
        entities.put(user.getId(), user);
        return true;
    }

    @Nullable
    private User insert(@Nullable final String userId, @Nullable final String login, @Nullable final String password, @Nullable RoleType roleType) {
        if (userId == null || login == null || password == null) return null;
        final User user = new User();
        user.setId(userId);
        user.setLogin(login);
        user.setPassword(password);
        if (roleType == null) user.setRoleType(RoleType.ANONIM);
        else user.setRoleType(roleType);
        entities.put(userId, user);
        return user;
    }

    @Override
    @Nullable
    public User findUserByLoginAndPassword(@NotNull final String login, @NotNull final String passwordMD5) {
        final User user = findByLogin(login);
        if (user == null) return null;
        if (passwordMD5.equals(user.getPassword())) return user;
        return null;
    }

    @Override
    public boolean insertAdmin(@NotNull final String login, @NotNull final String password) {
        if (findByLogin(login) != null) return false;
        final User user = new User();
        if (entities.containsKey(user.getId())) return false;
        user.setRoleType(RoleType.ADMINISTRATOR);
        user.setLogin(login);
        user.setPassword(password);
        entities.put(user.getId(), user);
        return true;
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    public boolean remove(@NotNull final String id) {
        return entities.remove(id) != null;
    }

    @Override
    public boolean persist(@NotNull final User user) {
        if (entities.containsKey(user.getId()))
            return update(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
        return insert(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType()) != null;
    }

    @Override
    public boolean merge(@NotNull final User user) {
        if (entities.get(user.getId()) == null)
            insert(user.getLogin(), user.getPassword());
        else
            update(user.getId(), user.getLogin(), user.getPassword(), user.getRoleType());
        return false;
    }

    @Nullable
    private User findByLogin(@NotNull String login) {
        for (User user : entities.values()) if (login.equals(user.getLogin())) return user;
        return null;
    }

    @Override
    public boolean update(@NotNull final String id, @NotNull final String login, @NotNull final String password, @Nullable RoleType roleType) {
        final User user = entities.get(id);
        if (user == null) return false;
        user.setLogin(login);
        user.setPassword(password);
        if (roleType == null) user.setRoleType(RoleType.ANONIM);
        else user.setRoleType(roleType);
        return true;
    }

    @Override
    public boolean update(@NotNull final String id, @NotNull final String login, @NotNull final String password) {
        final User user = entities.get(id);
        if (user == null) return false;
        user.setLogin(login);
        user.setPassword(password);
        return true;
    }

    @Override
    public List<User> getList() {
        return new ArrayList<>(entities.values());
    }
}
