package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.entity.Task;

import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.*;

@XmlSeeAlso(ArrayList.class)
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public boolean insert(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskName,
                          @NotNull final String descriptionTask, @Nullable final Date dateBeginTask,
                          @Nullable final Date dateEndTask) {
        Task newTask = new Task();
        if (entities.containsKey(newTask.getId())) return false;
        newTask.setUserId(userId);
        newTask.setName(taskName);
        newTask.setProjectId(projectId);
        newTask.setDescription(descriptionTask);
        newTask.setDateBeginTask(dateBeginTask);
        newTask.setDateEndTask(dateEndTask);
        entities.put(newTask.getId(), newTask);
        return true;
    }

    @Override
    public boolean persist(@NotNull final Task task) {
        if (task.getUserId() == null || task.getProjectId() == null) return false;
        return insert(task.getUserId(), task.getProjectId(), task.getId(), task.getName(), task.getDescription(),
                task.getDateBeginTask(), task.getDateEndTask()) != null;
    }

    @Nullable
    private Task insert(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId,
                        @Nullable final String name, @Nullable final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        if (userId == null || projectId == null || name == null) return null;
        final Task task = new Task();
        task.setUserId(userId);
        task.setProjectId(projectId);
        task.setId(taskId);
        task.setName(name);
        task.setDescription(description);
        task.setDateBeginTask(dateBegin);
        task.setDateEndTask(dateEnd);
        entities.put(taskId, task);
        return task;
    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String idUser, @NotNull final String idTask) {
        return findById(idUser, idTask);
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String idTask, @NotNull final String projectId,
                          @NotNull final String name, @Nullable final String description, @Nullable final Date dateBegin,
                          @Nullable final Date dateEnd) {
        final Task task = findById(userId, idTask);
        if (task == null) return false;
        if (!userId.equals(task.getUserId())) return false;
        task.setProjectId(projectId);
        task.setName(name);
        task.setDateEndTask(dateEnd);
        task.setDateBeginTask(dateBegin);
        task.setDescription(description);
        return true;
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) {
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId()) && taskId.equals(task.getId())) {
                entities.remove(taskId);
                return true;
            }
        return false;
    }

    @Override
    public boolean removeByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        final List<String> listTaskId = findTaskIdByProjectId(userId, projectId);
        if (listTaskId.isEmpty()) return true;
        for (final String str : listTaskId) entities.remove(str);
        return true;
    }

    @Nullable
    private Task findById(@NotNull final String userId, @NotNull final String taskId) {
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId()) && taskId.equals(task.getId())) return task;
        return null;
    }

    @NotNull
    private List<String> findTaskIdByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        final List<String> tasksIdForFind = new ArrayList<>();
        for (final Task task : entities.values())
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId()))
                tasksIdForFind.add(task.getId());
        return tasksIdForFind;
    }

    @Override
    public void removeAll(@NotNull final String idUser) {
        for (final Task task : entities.values()) if (idUser.equals(task.getUserId())) entities.remove(task.getId());
    }

    @NotNull
    @Override
    public Collection<Task> findAll(@NotNull final String userId) {
        final Collection<Task> collection = new ArrayList<>();
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId())) collection.add(task);
        return collection;
    }

    @Override
    public boolean merge(@NotNull final Task task) {
        if (task.getUserId() == null || task.getProjectId() == null) return false;
        if (entities.get(task.getId()) == null)
            insert(task.getUserId(), task.getProjectId(), task.getName(), task.getDescription(),
                    task.getDateBeginTask(), task.getDateEndTask());
        else
            update(task.getUserId(), task.getId(), task.getProjectId(), task.getName(), task.getDescription(),
                    task.getDateBeginTask(), task.getDateEndTask());
        return false;
    }

    @Override
    public @NotNull Collection<Task> findAllInOrder(@NotNull final String idUser, @NotNull final Comparator<Task> comparator) {
        TreeSet<Task> treeSet = new TreeSet<>(comparator);
        treeSet.addAll(findAll(idUser));
        return treeSet;
    }

    @Override
    public @NotNull Collection<Task> findByPartNameOrDescription(@NotNull final String idUser, @NotNull final String part) {
        Collection<Task> collection = new ArrayList<>();
        for (Task task : entities.values()) {
            if (task.getDescription() == null) {
                if (task.getName().contains(part)) collection.add(task);
            } else if (task.getName().contains(part) || task.getDescription().contains(part))
                collection.add(task);
        }
        return collection;
    }

    public List<Task> getList() {
        return new ArrayList<>(entities.values());
    }
}

