package ru.chistyakov.tm.api;

import ru.chistyakov.tm.entity.AbstractEntity;

public interface IRepository<T extends AbstractEntity> {

    boolean persist(T t);

    boolean merge(T t);
}
