package ru.chistyakov.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;

import java.util.Collection;

public interface IProjectService extends IService<Project> {

    boolean insert(@Nullable String userId, @Nullable String name, @Nullable String descriptionProject,
                   @Nullable String dateBegin, @Nullable String dateEnd);

    boolean merge(@Nullable Project project);

    void removeAll(@Nullable String userId);

    boolean persist(@Nullable Project project);

    boolean remove(@Nullable String userId, @Nullable String projectId);

    @Nullable
    Project findOne(@Nullable String userId, @Nullable String idProject);

    boolean update(@Nullable String projectId, @Nullable String projectName, @Nullable String descriptionProject, @Nullable String dateBegin, @Nullable String dateEnd);

    @Nullable
    Collection<Project> findAll(String userId);

    @Nullable
    Collection<Project> findAllInOrderDateBegin(@Nullable String userId);

    @Nullable
    Collection<Project> findAllInOrderDateEnd(@Nullable String userId);

    @Nullable
    Collection<Project> findAllInOrderReadinessStatus(@Nullable String userId);

    @Nullable
    Collection<Project> findByPartNameOrDescription(@Nullable String userId, String part);

}