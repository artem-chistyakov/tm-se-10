package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    boolean insert(@NotNull String userId, @NotNull String projectId, @NotNull String taskName,
                   @Nullable String descriptionTask, @Nullable Date dateBeginTask, @Nullable Date dateEndTask);

    boolean persist(@NotNull Task task);

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String taskId);

    boolean update(@NotNull String userId, @NotNull String taskId, @NotNull String projectId, @NotNull String newName,
                   @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    @NotNull
    Collection<Task> findAllInOrder(@NotNull String idUser, Comparator<Task> comparator);

    boolean remove(@NotNull String userId, @NotNull String taskId);

    boolean removeByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAll(@NotNull String userId);

    @NotNull
    Collection<Task> findAll(@NotNull String userId);

    boolean merge(@NotNull Task task);

    @NotNull
    Collection<Task> findByPartNameOrDescription(String idUser, String part);

    Collection<Task> findAll();

    List<Task> getList();
}
