package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;

public interface IService<T> {

    boolean merge(T t);

    boolean persist(T t);

    @NotNull
    Collection<T> findAll();
}
