package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.User;

public interface IUserService extends IService<User> {

    @Nullable
    User authoriseUser(@NotNull String name, @NotNull String password);

    boolean updateUser(@NotNull String userId, @NotNull String login, @NotNull String password);

    boolean updatePassword(@NotNull User user, @NotNull String password);

    boolean registryAdmin(@NotNull String login, @NotNull String password);

    boolean registryUser(@NotNull String login, @NotNull String password);
}
