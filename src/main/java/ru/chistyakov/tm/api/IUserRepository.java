package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {

    boolean insert(@NotNull String login, @NotNull String password);

    boolean insertAdmin(@NotNull String login, @NotNull String password);

    @Nullable
    User findOne(@NotNull String id);

    boolean remove(@NotNull String id);

    boolean persist(@NotNull User user);

    boolean merge(@NotNull User user);

    boolean update(@NotNull String id, @NotNull String login, @NotNull String password, @Nullable RoleType roleType);

    boolean update(@NotNull String id, @NotNull String login, @NotNull String password);

    @Nullable
    User findUserByLoginAndPassword(@NotNull String login, @NotNull String passwordMD5);

    @NotNull
    Collection<User> findAll();

    @Nullable
    List<User> getList();
}
