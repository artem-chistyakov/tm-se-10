package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserRepository;
import ru.chistyakov.tm.api.IUserService;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.utility.PasswordParser;

import java.util.Collection;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean merge(@Nullable final User user) {
        if (user == null) return false;
        return userRepository.merge(user);
    }

    @Override
    public boolean persist(@Nullable final User user) {
        if (user == null) return false;
        return userRepository.persist(user);
    }

    @Override
    @Nullable
    public User authoriseUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return null;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return null;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return null;
        return userRepository.findUserByLoginAndPassword(login, passwordMD5);
    }

    @Override
    public boolean updateUser(@Nullable final String userId, @Nullable final String login, @Nullable final String password) {
        if (userId == null || login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        return userRepository.update(userId, login, passwordMD5);
    }

    @Override
    public boolean updatePassword(@Nullable final User user, @Nullable final String password) {
        if (user == null || password == null) return false;
        if (password.trim().isEmpty()) return false;
        updateUser(user.getId(), user.getLogin(), password);
        return true;
    }

    @Override
    public boolean registryAdmin(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        return userRepository.insertAdmin(login, passwordMD5);
    }

    @Override
    public boolean registryUser(@Nullable final String login, @Nullable final String password) {
        if (login == null || password == null) return false;
        if (login.trim().isEmpty() || password.trim().isEmpty()) return false;
        final String passwordMD5 = PasswordParser.parse(password);
        if (passwordMD5 == null) return false;
        return userRepository.insert(login, passwordMD5);

    }

    @NotNull
    public Collection<User> findAll() {
        return userRepository.findAll();
    }
}
