package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.comporator.ProjectDateBeginComparator;
import ru.chistyakov.tm.comporator.ProjectDateEndComparator;
import ru.chistyakov.tm.comporator.ProjectReadinessStatusComparator;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.utility.DateParser;

import java.util.Collection;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public boolean merge(@Nullable final Project project) {
        if (project == null) return false;
        return projectRepository.merge(project);
    }

    @Override
    public boolean persist(@Nullable final Project project) {
        if (project == null) return false;
        return projectRepository.persist(project);
    }


    @Override
    public boolean insert(@Nullable final String userId, @Nullable final String login,
                          @Nullable String descriptionProject, @Nullable String dateBeginProject,
                          @Nullable String dateEndProject) {
        if (userId == null || login == null) return false;
        if (login.trim().isEmpty() || userId.trim().isEmpty()) return false;
        return projectRepository.insert(
                userId, login, descriptionProject,
                DateParser.parseDate(simpleDateFormat, dateBeginProject),
                DateParser.parseDate(simpleDateFormat, dateEndProject));
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null) return;
        if (userId.trim().isEmpty()) return;
        projectRepository.removeAll(userId);
        taskRepository.removeAll(userId);
    }

    @Override
    public boolean remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return false;
        if (projectId.trim().isEmpty()) return false;
        return projectRepository.remove(userId, projectId) && taskRepository.removeByProjectId(userId, projectId);
    }

    @Override
    @Nullable
    public Project findOne(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || projectId == null) return null;
        return projectRepository.findOne(projectId, userId);
    }


    @Override
    public boolean update(@Nullable final String projectId, @Nullable final String projectNameProject,
                          @Nullable final String descriptionProject, @Nullable final String dateBegin,
                          @Nullable final String dateEnd) {
        if (projectId == null || projectNameProject == null) return false;
        if (projectId.trim().isEmpty() || projectNameProject.trim().isEmpty()) return false;
        return projectRepository.update(projectId, projectNameProject, descriptionProject,
                DateParser.parseDate(simpleDateFormat, dateBegin),
                DateParser.parseDate(simpleDateFormat, dateEnd));
    }

    @Nullable
    @Override
    public Collection<Project> findAll(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findAll(userId);
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderDateBegin(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findAllInOrder(userId, new ProjectDateBeginComparator());
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderDateEnd(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findAllInOrder(userId, new ProjectDateEndComparator());
    }

    @Nullable
    @Override
    public Collection<Project> findAllInOrderReadinessStatus(@Nullable final String userId) {
        if (userId == null) return null;
        return projectRepository.findAllInOrder(userId, new ProjectReadinessStatusComparator());
    }

    @Nullable
    @Override
    public Collection<Project> findByPartNameOrDescription(@Nullable final String idUser, @Nullable final String part) {
        if (idUser == null || part == null) return null;
        return projectRepository.findByPartNameOrDescription(idUser, part);
    }

    @NotNull
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }
}
