package ru.chistyakov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Task;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "tasks")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Tasks {


    @XmlElement(name = "task")
    private List<Task> tasks = null;

    @Nullable
    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
