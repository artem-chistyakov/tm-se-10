package ru.chistyakov.tm.wrapper;

import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Project;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "projects")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Projects {

    @XmlElement(name = "project")
    private List<Project> projects = null;

    @Nullable
    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
